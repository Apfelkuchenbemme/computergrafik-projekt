using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Class taken from the first part of this video:
///     https://www.youtube.com/watch?v=b1uoLBp2I1w
/// 
/// Implements movement of a player character, where the player character has been given a Rigidbody
/// and will interact physically with the game world by e.g. pushing other objects around.
/// 
/// When implementing the "don't jump in mid-air" functionality the way we did below, you have to take
/// care not to set walls on the "FloorLayer", too, because then the player is going to kind of float
/// on walls.
/// </summary>
public class RigidbodyMovement : MonoBehaviour
{
    private Vector3 PlayerMovementInput;
    private Vector2 PlayerMouseInput;
    private float xRot;

    [SerializeField] private LayerMask FloorMask;
    [SerializeField] private Transform Feetransform;    //  Only really required to avoid jumping in mid-air
    [SerializeField] private Transform PlayerCamera;
    [SerializeField] private Rigidbody PlayerBody;
    [Space]
    [SerializeField] private float Speed;
    [SerializeField] private float Sensitivity;
    [SerializeField] private float JumpForce;

    private void Update()
    {
        PlayerMovementInput = new Vector3(Input.GetAxis("Horizontal"), 0f, Input.GetAxis("Vertical"));
        PlayerMouseInput = new Vector2(Input.GetAxis("Mouse X"), Input.GetAxis("Mouse Y"));

        MovePlayer();
        MovePlayerCamera();
    }

    private void MovePlayer()
    {
        Vector3 MoveVector = transform.TransformDirection(PlayerMovementInput) * Speed;
        PlayerBody.velocity = new Vector3(MoveVector.x, PlayerBody.velocity.y, MoveVector.z);

        //  Allow player to jump via spacebar
        if(Input.GetKeyDown(KeyCode.Space))
        {
            if (Physics.CheckSphere(Feetransform.position, 0.1f, FloorMask))
            {
                PlayerBody.AddForce(Vector3.up * JumpForce, ForceMode.Impulse);
            }
        }
    }

    private void MovePlayerCamera()
    {
        xRot -= PlayerMouseInput.y * Sensitivity;

        transform.Rotate(0f, PlayerMouseInput.x * Sensitivity, 0f);
        PlayerCamera.transform.localRotation = Quaternion.Euler(xRot, 0f, 0f);
    }

}
