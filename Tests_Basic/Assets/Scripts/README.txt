

    The Scripts:
        System > Controller.cs
        System > PoolSystem.cs
        SimpleCameraController.cs
    
    are straight up copied over from the "Essentials of real-time audio" tutorial from Unity, found here:
        https://learn.unity.com/project/essentials-of-real-time-audio
    
    Don't forget to go to:
        <<Unity Project>> - Edit - Project Settings - Input Manager:
            *   add Axis
            *   Name:               "Run"
            *   Negative Button:    "left shift"
            *   Positive Button:    ""