
    Scripts:
        System > Controller.cs
        System > PoolSystem.cs
        SimpleCameraController.cs
    
    are straight up copied over from the "Essentials of real-time audio" tutorial from Unity, found here:
        https://learn.unity.com/project/essentials-of-real-time-audio
    
    Don't forget to go to:
        <<Unity Project>> - Edit - Project Settings - Input Manager:
            *   add Axis
            *   Name:               "Run"
            *   Negative Button:    "left shift"
            *   Positive Button:    ""
    
    Add Gameobject named "Character" to the World.
    Add Gameobject named "CharacterRoot" to the Gameobject "Character".
    Move "Main Camera" into "CharacterRoot".
    Reset the position of "Main Camera".
    Add script "Controller" to "Character".
    Add component "Character Controller" to "Character".
    Set "Main Camera" of "Character" to the "Main Camera".
    Set "Camera Position" of "Character" to "CharacterRoot".
    
    
    Managed to "animate" the bandit by adding the .prefab to the "Character" object,
    Added the "Button Function"-script to the "Character" object, with the "Character" as Animator,
    Added the Animator of the Bandit to the "Character"