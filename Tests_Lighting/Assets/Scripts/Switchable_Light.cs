using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/**
 *  Class to give a sort of "light switch" functionality to some object.
 *  
 *  The object this script is added to doesn't necessarily have to be a light, the important things are:
 *      1.  Add a light to the script
 *      2.  Give the player object (e.g. a GameObject called "Character") the tag "Player"
 *      3.  Give the object that gets this script a collider
 **/
public class Switchable_Light : MonoBehaviour
{

    //  Light that gets switched on/off with this script
    public GameObject _light;

    //  Should get called once per frame, but is working a bit sluggish
    private void OnTriggerStay(Collider other)
    {
        if (other.tag == "Player" && Input.GetKeyDown(KeyCode.E))
        {
            _light.SetActive(!_light.activeSelf);
            Debug.Log("Clicked E");
        }
            
    }
}
